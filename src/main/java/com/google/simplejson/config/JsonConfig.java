package com.google.simplejson.config;

public class JsonConfig {

  private final String name;
  private String location;

  public JsonConfig(String name) {
    this.name = name;
  }

  public JsonConfig(String name, String location) {
    this.name = name;
    this.location = location;
  }

  public String getName() {
    return name;
  }

  public String getLocation() {
    return location;
  }
}
