package com.google.simplejson.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public final class JsonConfigRepository {

  private final Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
  private final HashMap<String, JsonConfig> jsonConfigs = new HashMap<>();

  public JsonConfig loadOrCreate(JsonConfig jsonConfig) {
    JsonConfig out = jsonConfig;
    // path selection threw our configuration arguments
    Path path = jsonConfig.getLocation() != null ? Paths.get(jsonConfig.getLocation() + "/" + jsonConfig.getName()) : Paths.get(jsonConfig.getName());

    try {
      // create new reader for our file
      Reader reader = Files.newBufferedReader(path);
      // read values from our config
      out = gson.fromJson(reader, jsonConfig.getClass());
      // close file "editor"
      reader.close();
    } catch (IOException e) {
      // create save call cause our config isn't available
      try {
        save(jsonConfig);
      } catch (IOException ex) {
        ex.printStackTrace();
      }
    }

    return out;
  }

  public void save(JsonConfig jsonConfig) throws IOException {
    // path selection threw our configuration arguments
    Path path = jsonConfig.getLocation() != null ? Paths.get(jsonConfig.getLocation() + "/" + jsonConfig.getName()) : Paths.get(jsonConfig.getName());

    // create directories if they don't exist
    if (jsonConfig.getLocation() != null) Files.createDirectories(Paths.get(jsonConfig.getLocation()));

    // create new writer for our file
    Writer writer = Files.newBufferedWriter(path);
    // parse values from our config
    gson.toJson(jsonConfig, writer);
    // close file "editor"
    writer.close();
  }

  public Map.Entry<String, JsonConfig> by(Class<? extends JsonConfig> configClass) {
    // search threw the entrySet of our hashMap
    for (Map.Entry<String, JsonConfig> config : jsonConfigs.entrySet()) {
      // check if the specified class is the same as the config class
      if (config.getValue().getClass() == configClass)
        return config;
    }
    return null;
  }

  public HashMap<String, JsonConfig> jsonConfigs() {
    return jsonConfigs;
  }

  public Gson gson() {
    return gson;
  }


}
