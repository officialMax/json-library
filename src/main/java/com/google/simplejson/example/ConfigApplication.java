package com.google.simplejson.example;

import com.google.simplejson.config.JsonConfigRepository;

public final class ConfigApplication {

  private static ConfigApplication application;

  private final JsonConfigRepository jsonConfigRepository = new JsonConfigRepository();
  private ExampleConfig exampleConfig;

  public ConfigApplication() {
    application = this;
  }

  public void start() throws InterruptedException {
    this.exampleConfig = (ExampleConfig) this.jsonConfigRepository.loadOrCreate(new ExampleConfig());

    System.out.println(this.exampleConfig.test);
  }

  public static ConfigApplication getApplication() {
    return application;
  }
}
