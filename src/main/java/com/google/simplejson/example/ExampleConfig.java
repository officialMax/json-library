package com.google.simplejson.example;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.simplejson.config.JsonConfig;

public class ExampleConfig extends JsonConfig {

  @Expose
  @SerializedName("test")
  public String test = "Hello!";

  public ExampleConfig() {
    super("locations", "example.json");
  }
}
