package com.google.simplejson.example;

public class Start {

  public static void main(String[] args) {
    ConfigApplication application = new ConfigApplication();
    try {
      application.start();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}
